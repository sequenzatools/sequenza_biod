

module sequenza.utils.acgt;

import std.ascii;
import std.algorithm;
import std.range;
import std.random;
import std.traits;
import std.conv;
import std.array;
import std.exception;

///
struct Acgt_strand {
    /// Dictionary with number of occurrence of A, C, G and T
    int[4] counts;
    /// array with the n bases in hte forward strand, index order is A,C,G and T
    int[4] strand;
}

////
auto acgt(P, Q) (P pileup,
              Q quality,
              int depth,
              char reference,
              int qlimit = 53) {

    int[char] ret;
    
    //int[] quality;
    //string pileup;


    //har reference;
    //int depth, qlimit, i;
    //qlimit    = 53;
    //int[4] nucleot_list = pileup_parse(pileup, quality, depth,
    Acgt_strand nucleot_list = pileup_parse(pileup, quality, depth,
        reference, qlimit);
    //ret['A'] = nucleot_list[0];
    //ret['C'] = nucleot_list[1];
    //ret['G'] = nucleot_list[2];
    //ret['T'] = nucleot_list[3];
    //ret['Z'] = strand_list;
    return nucleot_list;
}
    



int get_index(char a)
{
    int i = 0;
    if (a == 'A') {
        i = 0;
    } else if (a == 'C') {
        i = 1;
    } else if (a == 'G') {
        i = 2;
    } else if (a == 'T') {
        i = 3;
    } else if (a == 'a') {
        i = 4;
    } else if (a == 'c') {
        i = 5;
    } else if (a == 'g') {
        i = 6;
    } else if (a == 't') {
        i = 7;
    } else if (a == '$') {
        i = 8;
    } else if (a == '*') {
        i = 9;
    } else if (a == 'N') {
        i = 10;
    } else if (a == 'n') {
        i = 11;
    } else if (a == '-') {
        i = 12;
    } else if (a == '+') {
        i = 13;
    }
    return i;
}

////
int is_number (char a) {
    int i = 0;
    if (a == '0') {
        i = 1;
    } else if (a == '1') {
        i = 1;
    } else if (a == '2') {
        i = 1;
    } else if (a == '3') {
        i = 1;
    } else if (a == '4') {
        i = 1;
    } else if (a == '5') {
        i = 1;
    } else if (a == '6') {
        i = 1;
    } else if (a == '7') {
        i = 1;
    } else if (a == '8') {
        i = 1;
    } else if (a == '9') {
        i = 1;
    }
    return i;
}


///
auto pileup_parse(string pileup, int[] quality, int depth, char reference,
    int qlimit)
{
    int[4] nucleot_list = [0, 0, 0, 0];
    int[4] strand_list = [0, 0, 0, 0];
    char rev_reference;
    int n, q, last_base, offset, indel, step;
    n = 0;
    q = 0;
    last_base = -1;

   rev_reference = toLower(reference);

    //while(pileup[n] != '\0') {
    while(n < pileup.length) {
        char base = pileup[n];
        if (base == '^') {
                n += 2;
                base = pileup[n];
        }
        if (base == '.') {
            base = reference;
        } else if( base == ',') {
            base = rev_reference;
        }
        int index = get_index(base);
        int qual = quality[q];
        if (index < 4){
            if (qual >= qlimit) {
                last_base = index;
                nucleot_list[index] += 1;
                strand_list[index] += 1;
            } else {
                last_base = -1;
            }
            n += 1;
            q += 1;
        } else if (index < 8) {
            if (qual >= qlimit) {
                last_base = index;
                nucleot_list[index - 4] += 1;
            } else {
                last_base = -1;
            }
            n += 1;
            q += 1;
        } else if (index == 8) {
            last_base = -1;
            n += 1;
        } else if (index >= 9 && index <= 11) {
            last_base = -1;
            n += 1;
            q += 1;
        } else if (index > 11) {
            offset = n + 1;
            indel = 0;
            for(;;) {
                if (is_number(pileup[offset])) {
                    if (indel == 0) {
                        step = pileup[offset] - '0';
                    } else {
                        int pile_offset = pileup[offset] - '0';
                        step = (step * 10) + pile_offset;
                    }
                    indel += 1;
                    offset += 1;
                } else {
                    break;
                }
            }
            n = step + offset;
            indel = 0;
        }
    }

    //return nucleot_list;
    Acgt_strand res;
    res.counts = nucleot_list;
    res.strand = strand_list;
    return res;
}