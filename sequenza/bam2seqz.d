
module sequenza.bam2seqz;

//import bio.std.hts.bam.multireader;
import bio.std.hts.bam.reader;
import bio.std.hts.bam.read : compareCoordinates;
//import bio.std.hts.bam.pileup;
//import bio.std.experimental.hts.pileup;
import sequenza.utils.pileup;
import sequenza.utils.acgt;

//import bio.std.experimental.hts.bam.reader;

import std.exception;
import std.getopt;
import std.algorithm;
import std.conv;
import std.stdio;
import std.string;

/// Print CLI help message
void printUsage() {
    stderr.writeln("Usage: sequenza-utils bam2seqz [options]
Options: -n, --normal=NORMAL
                    Normal BAM file
         -t, --tumor=TUMOR
                    Tumor BAM file
         -q, --quality=Q
                    Quality Treshold, default 20");
}

/// 
int bam2seqz_main(string[] args) {

    string normal;
    string tumor;
    int quality = 20;
        if (args.length < 2) {
            printUsage();
            return 0;
        }

        getopt(args,
            std.getopt.config.caseSensitive,
            "tumor|t",            &tumor,
            "normal|n",           &normal,
            "quality|q",          &quality);

         // multiple BAM files can be traversed simultaneously (if they can be merged)
        auto tumor_bam = new BamReader(tumor);
        auto normal_bam = new BamReader(normal);
        // auto bam = new MultiBamReader([normal]);
        //auto bam = new BamReader(normal);
        //auto bam = BamReadBlobs(normal);

        writeln(quality);
        auto header = tumor_bam.header;
        string[] samples;
        foreach (rg_line; header.read_groups) {
            //auto rg_line = header.read_groups[i];
            //samples[i] = rg_line.sample;
            samples ~= rg_line.sample;

        }
        auto tumor_pileup = makePileup(tumor_bam.reads, false, 0, ulong.max, false); //, true);// ANY range of reads is acceptable
        auto normal_pileup = makePileup(normal_bam.reads, false, 0, ulong.max, false); //, true);// ANY range of reads is acceptable

        writeln(samples);
        foreach (sq_line; header.sequences) {
            writeln(sq_line.name);
        }

        foreach (column; tumor_pileup) {
        //foreach (read; tumor_bam.reads) {
            auto rg_reads = column.reads;
            //writeln(rg_reads);
            //foreach (read; rg_reads) {
            //    writeln(read["RG"]);
            //}
            writeln("    ref_id: ", column.ref_id);
            writeln("    chromosome: ", header.sequences.getSequence(column.ref_id).name);
            writeln("    Ref.base: ", column.reference_base);
            writeln("    Coverage: ", column.coverage);
            writeln("Column position: ", column.position);

            writeln("    ", column.bases);
            writeln("    ", column.base_qualities);
            if (column.coverage > 0){
                char refn = 'N';
                int depth = 1;
                int[] quals;
                string bases = to!string(column.bases);
                foreach(qual; column.base_qualities) {
                    quals ~= to!int(qual);
                }
                Acgt_strand acgd_dict = acgt(bases, quals, depth, refn, quality);
                writeln(acgd_dict.counts);
                writeln(acgd_dict.strand);
            }
        
        }

       /*auto bam = BamReadBlobStream(normal);

       writeln(bam.header.text);
       writeln(bam.header.refs);
       while(!bam.stream.eof()) {
           auto read = ProcessReadBlob(bam.front());
           writeln(bam.header.refs[read.raw_ref_id].name);
           writeln(read.sequence());
           bam.popFront();
       }*/

    return 0;
}
