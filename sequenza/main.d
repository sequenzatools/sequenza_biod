/*
    This file is part of sequenza-utils.
    Copyright (C) 2014-2019   Francesco Favero <favero.francesco@gmail.com>

    Sequenza-utils is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Sequenza-utils is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

import std.algorithm;
import std.experimental.logger;
import std.range.primitives;

import sequenza.bam2seqz;

import utils.version_ : VERSION;
import utils.ldc_version_info_ : LDC_VERSION_STRING, DMD_VERSION_STRING, LLVM_VERSION_STRING, BOOTSTRAP_VERSION_STRING;


import std.stdio;

///
void printUsage() {
    stderr.writeln("
Usage: sequenza-utils [command] [args...]

  Available commands:

    bam2seqz        view contents and convert from one format
                to another (SAM/BAM/CRAM/JSON/UNPACK)

To get help on a particular command, call it without args.

");
}

///
void printVersion() {
    stderr.writeln();
    stderr.writeln("sequenza-utils " ~ VERSION ~ " by Francesco Favero (C) 2014-2019");
    stderr.writeln("    LDC " ~ LDC_VERSION_STRING ~ " / DMD " ~ DMD_VERSION_STRING ~
     " / LLVM" ~ LLVM_VERSION_STRING ~ " / bootstrap " ~ BOOTSTRAP_VERSION_STRING);
    stderr.writeln();
}

int main(string[] args) {
    globalLogLevel(LogLevel.info);
    if (args.find("-q").empty)
      printVersion();

    auto args2 = args.remove!(a => a == "-q");
    if (args2.length == 1) {
        printUsage();
        return 1;
    }

    auto _args = args2[0] ~ args2[2 .. $];

    switch (args[1]) {
        case "bam2seqz":      return bam2seqz_main(_args);
        default:
            printUsage();
            return 1;
    }
}
