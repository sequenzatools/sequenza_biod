# This is a minimalistic make file to build sequenza-utils with ldc2 as per instructions on
#
# Targets (64-bit):
#
#   Linux
#   OSX
#
# Typical usage:
#
#   make LIBRARY_PATH=~/opt/ldc2-$ver-linux-x86_64/lib debug|profile|release|static
#
# Static release with optimization (for releases):
#
#   make LIBRARY_PATH=~/opt/ldc2-$ver-linux-x86_64/lib pgo-static
#
# Debug version
#
#   make LIBRARY_PATH=~/opt/ldc2-$ver-linux-x86_64/lib debug
#

D_COMPILER=ldc2

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
  SYS = OSX
else
  SYS = LINUX
endif

DFLAGS      = -wi -I. -IBioD -g

DLIBS       = $(LIBRARY_PATH)/libphobos2-ldc.a $(LIBRARY_PATH)/libdruntime-ldc.a
DLIBS_DEBUG = $(LIBRARY_PATH)/libphobos2-ldc-debug.a $(LIBRARY_PATH)/libdruntime-ldc-debug.a
#LIBS        = htslib/libhts.a lz4/lib/liblz4.a -L-L$(LIBRARY_PATH) -L-lpthread -L-lm
#LIBS_STATIC = $(LIBRARY_PATH)/libc.a $(DLIBS) htslib/libhts.a $(LIBRARY_PATH)/liblz4.a
SRC         = utils/ldc_version_info_.d $(wildcard BioD/contrib/undead/*.d BioD/contrib/undead/*/*.d) utils/version_.d $(sort $(wildcard BioD/bio/*/*.d BioD/bio/*/*/*.d BioD/bio/*/*/*/*.d BioD/bio/*/*/*/*/*.d) $(wildcard sequenza/*.d sequenza/*/*.d sequenza/*/*/*.d))
OBJ         = $(SRC:.d=.o)
OUT         = bin/sequenza-utils

#STATIC_LIB_PATH=

.PHONY: all debug release static clean test

all: release

debug:                             DFLAGS += -O0 -d-debug -link-debuglib

profile:                           DFLAGS += -fprofile-instr-generate=profile.raw

release static profile pgo-static: DFLAGS += -O3 -release -enable-inlining -boundscheck=off

static:                            DFLAGS += -static -L-Bstatic

pgo-static:                        DFLAGS += -fprofile-instr-use=profile.data

utils/ldc_version_info_.d:
	python3 ./gen_ldc_version_info.py $(shell which ldmd2) > utils/ldc_version_info_.d
	cat utils/ldc_version_info_.d

ldc_version_info: utils/ldc_version_info_.d

build-setup: ldc_version_info
	mkdir -p bin/

default debug release static: $(OUT)

profile: release
	./bin/sequenza-utils sort /gnu/data/in_raw.bam -p > /dev/null
	ldc-profdata merge -output=profile.data profile.raw
	rm ./bin/sequenza-utils ./bin/sequenza-utils.o # trigger rebuild

default: all

# ---- Compile step
%.o: %.d
	$(D_COMPILER) $(DFLAGS) -c $< -od=$(dir $@)

singleobj: build-setup
	$(info compile single object...)
	$(D_COMPILER) -singleobj $(DFLAGS) -c -of=bin/sequenza-utils.o $(SRC)

# ---- Link step
$(OUT): singleobj
	$(info linking...)
	$(D_COMPILER) $(DFLAGS) -of=bin/sequenza-utils bin/sequenza-utils.o $(LINK_OBJ) $(LIBS)

test:
	./run_tests.sh

check: test

debug-strip:
	objcopy --only-keep-debug bin/sequenza-utils sequenza-utils.debug
	objcopy --strip-debug bin/sequenza-utils
	objcopy --add-gnu-debuglink=sequenza-utils.debug bin/sequenza-utils
	mv sequenza-utils.debug bin/

pgo-static: profile static debug-strip

install:
	install -m 0755 bin/sequenza-utils $(prefix)/bin

clean: clean-d
	cd htslib ; $(MAKE) clean
	rm -f profile.data
	rm -f profile.raw

clean-d:
	rm -rf bin/*
	rm -vf utils/ldc_version_info_.d
	rm -f $(OBJ) $(OUT) trace.{def,log}
